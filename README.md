# Please help by citing this article and methods as follows:  
### Mukherjee S. Quiescent stem cell marker genes in glioma gene networks are sufficient to distinguish between normal and glioblastoma (GBM) samples. Sci Rep. 2020 Jul 2;10(1):10937. doi: 10.1038/s41598-020-67753-5. PMID: 32616845; PMCID: PMC7363816.  

Aim: To prediction of glioma grade based on gene expression value of signature adult neural stem cell genes 

Background and Significance: 
Gliomas are deadliest form of cancers. Traditionally histology and select number of genes are used to grade these tumors---grade 4 is most severe. Classification of tumor grade is essential to direct therapeutic pathway in treatment. Can we use gene expression data to more accurately classify tumors into grades or aggressiveness instead of relying on just the traditional genes? 
Stem cell theory of cancer proposes that stem cells are the origin and basis of cancer aggressiveness. So can “stem-like” genes be used to grade tumor? 

Methodology:
In the present project, adult neural stem cell (brain stem cells) signature genes will be utilized to deconvoluate stem cell signature from glioblastoma RNA-seq and predict glioma (brain tumor) grade. 

Application:
With the decreased costs of sequencing, WHO initiatives and efforts towards precision medicine, RNA-seq of cancers is quickly becoming an integral part of healthcare. A stem cell gene expression based predictive model will be developed that can be used to predict accurately the tumor grade of glioblastoma. This will facilitate clinical decisions, which depend on tumor grades, geared towards targeted and personalized medicine.   

Output Format: the pipelines are provided as 1)Rmarkdown files knit to html 2) Rmarkdown files knit to pdf and 3) Rmarkdown or rmd files that can be opened and run on Rstudio by just hitting the "run current chunk" button

This pipeline shows steps from input FPKM RNA-seq gene expression data-(to)->deconvolution of adult neural stem cell signature genes-(to)->combination with glioblastoma tumor grade-(to)->transformation, normalization and graphical visualidation of input stem cell gene expression in glioblastoma dependent glioblastoma tumor grade output. These results will be further utilized for predictive statistical modeling to predict tumor grade from bulk RNA-seq glioblastoma data. 

Details of obtained datasets: 
1)Data-mining from different sources (GSE68270, GSE70696, GSE52564) for RNA-seq of mouse adult neural stem cells (hippocampus) and other cell types from hippocampus+cortex (astrocytes, oligodendrocytes, neurons and microglia) to define adult neural stem cell signature genes (analysis unpublished, available upon request).
2)Human glioma gender, age, histology, RNA-seq and grade. Glioblastoma gene expression processed data (325 tomors RNA-seq, at NCBI-SRA site, SRP027383 and SRP091303) with available meta-data on tumor grade. Ref: Zhao et al 2017 Scientific Data










